//
//  HomePagerVC.swift
//  DragonFly
//
//  Created by Suthar on 06/01/17.
//  Copyright © 2017 Octal. All rights reserved.
//

import UIKit
import Alamofire

class HomePagerVC: UIViewController , UITableViewDelegate , UITableViewDataSource, UICollectionViewDelegate ,UICollectionViewDataSource {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var heightConstCollection: NSLayoutConstraint!
    
    var modelCollection : ModelCategory!
    var arrDeals = NSMutableArray()
    var isSelectCollectionCell : Bool?
    var intIndex : NSInteger?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.automaticallyAdjustsScrollViewInsets = false
        self.tblView.separatorColor = UIColor.clear
        
        isSelectCollectionCell = false
        
        arrDeals = NSMutableArray()
        self.tblView.register(UINib(nibName: "HomePagerCell", bundle: nil), forCellReuseIdentifier: "HomePagerCell")
        
        self.collectionView.register(UINib(nibName: "HomePagerCollectionCell", bundle: nil), forCellWithReuseIdentifier: "HomePagerCollectionCell")
        self.collectionView.reloadData()
    
    }
    
    func getDealsFromCategory (subCateGoryIndex : NSInteger, strSubCategoryIds : NSString) {
        
        if gLatitude == "" && gLongitude == "" {
            gLatitude = "26.9124"
            gLongitude = "75.7873"
        }
        if (Alamofire.NetworkReachabilityManager(host: "www.google.com")?.isReachable)! {
            load.show(views: self.view)
            
               let dict : Dictionary = ["user_id": defaults.object(forKey: "USERID"),"category_id":modelCollection.categoryId,"subcategory_Id":strSubCategoryIds,"lat":gLatitude,"long":gLongitude]
               print(dict.values)
            
            appConfig.fetchDataFromServer(header: "get_deals", withParameter: dict as NSDictionary, inVC: self) { (responce, staus) in
                load.hide(delegate: self)
                let status =  responce.value(forKey: "status")
                self.arrDeals.removeAllObjects()
                if (status! as AnyObject).doubleValue == 1.0 {
                    var DealsArr = NSMutableArray()
                    let DealsArrNew = responce["Deals"]! as! NSArray
                    DealsArr = NSMutableArray(array: DealsArrNew)
                    for dict in DealsArr {
                        let model = ModelDeals()
                        model.DealIDs   = (dict as! NSDictionary).value(forKey: "deal_Id") as? NSString
                        model.DealName  = (dict as! NSDictionary).value(forKey: "deal_Name") as? NSString
                        model.DealDesc  = (dict as! NSDictionary).value(forKey: "deal_desc") as? NSString
                        model.Distance  = (dict as! NSDictionary).value(forKey: "distance") as? NSString
                        model.ISFav     = NSString(format: "%@",(((dict as! NSDictionary).value(forKey: "is_fav")!) as AnyObject).description) //(dict as! NSDictionary).value(forKey: "is_fav") as? NSString
                        model.DealImg = NSString(format: "%@",(((dict as! NSDictionary).value(forKey: "deal_image")!) as AnyObject).description)
                        self.arrDeals.add(model)
                    }
                  // self.tblView.reloadData()
                }
                self.tblView.reloadData()
            }
        } else {
            load.hide(delegate: self)
            appConfig.presentAlertWithTitle(title: kAPPName, message: "No internet connection.", vc: self)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true
        
        if modelCollection.subCategoryArr?.count == 0 {
            heightConstCollection.constant = 0.0
        } else {
            heightConstCollection.constant = 50.0
        }
        if (modelCollection.subCategoryArr?.count)! > 0 {
            intIndex = 0
            let modelSubColl : ModelCategory = modelCollection.subCategoryArr![0] as! ModelCategory
            getDealsFromCategory (subCateGoryIndex : 0, strSubCategoryIds: modelSubColl.SubCategoryId!)

        } else {
            getDealsFromCategory (subCateGoryIndex : 0, strSubCategoryIds: "")
        }
        collectionView.reloadData()
    }
 
    // MARK: - Table view data source & delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 122.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDeals.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identiFier = "HomePagerCell"
        let modelTable = arrDeals[indexPath.row] as! ModelDeals
        let cell : HomePagerCell = tableView.dequeueReusableCell(withIdentifier: identiFier) as! HomePagerCell
        
        cell.lblDealTitle.text = modelTable.DealName as String?
        if modelTable.DealDesc == "" {
            
        }else{
            cell.lblDealDesc.text  = modelTable.DealDesc as String?
        }
        cell.lblDistance.text  = "\(modelTable.Distance!) Miles"//"(modelTable.Distance) M"
        if modelTable.ISFav == "1" {
            cell.btnFavoriate.isSelected = true
        } else {
            cell.btnFavoriate.isSelected = false
        }
        cell.btnFavoriate.tag = indexPath.row
        cell.btnFavoriate.addTarget(self, action: #selector(pressFavUnFavButton(button:)), for: .touchUpInside)
        cell.imgDeal.setImageWithUrl(NSURL(string: modelTable.DealImg as! String) as! URL, placeHolderImage: UIImage(named: "inner_slider"))
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        return cell
        
    }
    
    func pressFavUnFavButton(button: UIButton) {
        NSLog("pressed!")
        let modelTable = arrDeals[button.tag] as! ModelDeals
        if (Alamofire.NetworkReachabilityManager(host: "www.google.com")?.isReachable)! {
            load.show(views: self.view)
            let dict : Dictionary = ["user_id": defaults.object(forKey: "USERID"),"deal_id":modelTable.DealIDs!,"is_fav":modelTable.ISFav!]
            print(dict.values as Any)
            appConfig.fetchDataFromServer(header: "fav_deal", withParameter: dict as NSDictionary, inVC: self) { (responce, staus) in
                load.hide(delegate: self)
                let status =  responce.value(forKey: "status")
                if (status! as AnyObject).doubleValue == 1.0 {
                    let modelTable = self.arrDeals[button.tag] as! ModelDeals
                    modelTable.ISFav = NSString(format: "%@",(responce.value(forKey: "is_fav")! as AnyObject).description)
                    self.tblView.reloadData()
                }
            }
        } else {
            load.hide(delegate: self)
            appConfig.presentAlertWithTitle(title: kAPPName, message: "No internet connection.", vc: self)
        }        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       //appConfig.presentAlertWithTitle(title: kAPPName, message: "Under Development.", vc: self)
        
        let modelTable = arrDeals[indexPath.row] as! ModelDeals
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc : HomeDealDetailVC = storyboard.instantiateViewController(withIdentifier: "HomeDealDetailVC") as! HomeDealDetailVC
        vc.StrDealIds = modelTable.DealIDs!
        vc.navigationController?.isNavigationBarHidden = false
        vc.navigationItem.hidesBackButton = true
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    // MARK: - Collection view data source & delegate Methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (modelCollection.subCategoryArr?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if modelCollection.subCategoryArr?.count == 0 {
            return CGSize(width: 0, height: 0)
        }else {
            return CGSize(width: 95, height: 26)
        }
    }
  
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let modelSubColl : ModelCategory = modelCollection.subCategoryArr![indexPath.row] as! ModelCategory
        let cell : HomePagerCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomePagerCollectionCell", for: indexPath) as! HomePagerCollectionCell
        
        cell.lblCategoryName.text = modelSubColl.SubCategoryName as? String
        cell.lblCategoryName.clipsToBounds = true
        cell.lblCategoryName.layer.borderWidth = 1;
        cell.lblCategoryName.layer.borderColor = UIColor.lightGray.cgColor
        cell.lblCategoryName.layer.cornerRadius = 12
        if indexPath.row == intIndex {
            cell.lblCategoryName.backgroundColor = UIColor.black
            cell.lblCategoryName.textColor = UIColor.white
        } else {
            cell.lblCategoryName.backgroundColor = UIColor.white
            cell.lblCategoryName.textColor = UIColor.lightGray
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        intIndex = indexPath.row
        let modelSubColl : ModelCategory = modelCollection.subCategoryArr![indexPath.row] as! ModelCategory
        getDealsFromCategory (subCateGoryIndex : indexPath.row, strSubCategoryIds: modelSubColl.SubCategoryId!)
        collectionView.reloadData()
        
    }
  
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(7, 7, 7, 7)
    }
    
//    - (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
//    return 0.0;
//    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
