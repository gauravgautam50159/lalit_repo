//
//  HomeVC.swift
//  DragonFly
//
//  Created by Suthar on 05/01/17.
//  Copyright © 2017 Octal. All rights reserved.
//

import UIKit
import GooglePlaces
import GooglePlacePicker
import Alamofire

extension HomeVC: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        gCityName =   place.name
        lblNavLocationName.text = gCityName
        print("Place address: \(place.formattedAddress)")
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}

class HomeVC: UIViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var viewNavBar: UIView!
    @IBOutlet weak var lblNavLocationName: UILabel!
    
    var placesClient: GMSPlacesClient?
    var pageMenu : CAPSPageMenu?
    var arrCategory = NSMutableArray()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var coreLocationController:LocationGet?
    //var arrDealsList = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.coreLocationController     = LocationGet()
        NotificationCenter.default.addObserver(self, selector: #selector(HomeVC.locationAvailable(_:)), name: NSNotification.Name(rawValue: "LOCATION_AVAILABLEHOME"), object: nil)
        
        searchBar.isUserInteractionEnabled = false
        
        placesClient = GMSPlacesClient.shared()
        //GMSServices.provideAPIKey("AIzaSyBzs3yrj1l7xFVaO9hKnW75NTfnilKLO60")
        
        arrCategory = NSMutableArray()
        getCategoryListFromServer()
        //arrDealsList = NSMutableArray()
        
    }
    
    func locationAvailable(_ notification:Notification) -> Void
    {
        let userInfo = notification.userInfo as! Dictionary<String,String>
        gLatitude  = userInfo["latitude"]!
        gLongitude = userInfo["longitude"]!
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "LOCATION_AVAILABLEHOME"), object: nil);
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.menuContainerViewController.panMode = MFSideMenuPanModeDefault
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func getCategoryListFromServer () {
        if (Alamofire.NetworkReachabilityManager(host: "www.google.com")?.isReachable)! {
            load.show(views: self.view)
            let dict =  NSDictionary()
            appConfig.fetchDataFromServer(header: "get_category_list", withParameter: dict as NSDictionary, inVC: self) { (responce, staus) in
                load.hide(delegate: self)
                let status =  responce.value(forKey: "status")
                if (status! as AnyObject).doubleValue == 1.0 {
//                    self.arrCategory.removeAllObjects()
                    var CateArr = NSMutableArray()
                    let CateArrNew = responce["Category"]! as! NSArray
                    CateArr = NSMutableArray(array: CateArrNew)
                    for dict in CateArr {
                        let model = ModelCategory()
                        model.categoryName  = (dict as AnyObject).value(forKey: "name") as? NSString
                        model.categoryId  = (dict as AnyObject).value(forKey: "id") as? NSString
                        model.isSubCategory = (dict as AnyObject).value(forKey: "is_subcategory") as? NSString
                        model.subCategoryArr = NSMutableArray()
                        let status =  (dict as AnyObject).value(forKey: "is_subcategory")
                        if (status as AnyObject).doubleValue == 1.0 {
                            var arrSubCategory = NSMutableArray()
                            let SubCateArrNew = (dict as AnyObject).value(forKey: "Sub_Category") as! NSArray
                            arrSubCategory = NSMutableArray(array: SubCateArrNew)
                            for dictSubCate in arrSubCategory {
                                let modelSub = ModelCategory()
                                modelSub.SubCategoryId  = (dictSubCate as AnyObject).value(forKey: "id") as? NSString
                                modelSub.SubCateSelect = false
                                modelSub.SubCategoryName = (dictSubCate as AnyObject).value(forKey: "name") as? NSString
                                model.subCategoryArr?.add(modelSub)
                            }
                        }
                        self.arrCategory.add(model)
                    }
                    self.makeManuallyView()
                }
            }
        } else {
            load.hide(delegate: self)
            appConfig.presentAlertWithTitle(title: kAPPName, message: "No internet connection.", vc: self)
        }
    }
    
    func makeManuallyView () {
        
        if (defaults.object(forKey: "CityName") != nil) {
            gCityName = defaults.object(forKey: "CityName") as! String
            lblNavLocationName.text = gCityName
        }
        var controllerArray : [UIViewController] = []
        for model in arrCategory {
            let controller : HomePagerVC = HomePagerVC(nibName: "HomePagerVC", bundle: nil)
            controller.title = (model as! ModelCategory).categoryName as? String
            controller.modelCollection = model as! ModelCategory
            controllerArray.append(controller)
        }
        
        // Customize menu (Optional)
        let parameters: [CAPSPageMenuOption] = [
            .scrollMenuBackgroundColor(UIColor(red: 0.0/255.0, green: 136.0/255.0, blue: 206.0/255.0, alpha: 1.0)),
            .viewBackgroundColor(UIColor.clear),
            .selectionIndicatorColor(UIColor.clear),
            .bottomMenuHairlineColor(UIColor(red: 70.0/255.0, green: 70.0/255.0, blue: 80.0/255.0, alpha: 1.0)),
            .menuItemFont(UIFont(name: kAPPFontBold, size: 12.0)!),
            .menuHeight(40.0),
            .menuItemWidth(90.0),
            .centerMenuItems(true)
        ]
        
        // Initialize scroll menu
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: viewNavBar.frame.size.height+searchBar.frame.size.height, width: self.view.frame.width, height: self.view.frame.height-viewNavBar.frame.size.height+searchBar.frame.size.height), pageMenuOptions: parameters)
        
        self.addChildViewController(pageMenu!)
        self.view.addSubview(pageMenu!.view)
        pageMenu!.didMove(toParentViewController: self)
        
        //getDealsFromCategory(subCateGoryIndex : 0)
        
    }
    
    
//    func getDealsFromCategory (subCateGoryIndex : NSInteger) {
//        
//        if gLatitude == "" && gLongitude == "" {
//            gLatitude = "26.9124"
//            gLongitude = "75.7873"
//        }
//        if (Alamofire.NetworkReachabilityManager(host: "www.google.com")?.isReachable)! {
//            load.show(views: self.view)
//            let modelCate : ModelCategory = arrCategory[pageMenu!.currentPageIndex] as! ModelCategory
//            var strSubCategoryIds = ""
//            if (modelCate.subCategoryArr?.count)! > 0 {
//                let modelSubCate : ModelCategory = modelCate.subCategoryArr![subCateGoryIndex] as! ModelCategory
//                strSubCategoryIds = modelSubCate.SubCategoryId as! String
//            }
//            
//            //   let dict : Dictionary = ["user_id": defaults.object(forKey: "USERID"),"category_id":modelCate.categoryId,"subcategory_Id":strSubCategoryIds,"lat":gLatitude,"long":gLongitude]
//            //   print(dict.values)
//            
//            let dict : Dictionary = ["user_id": defaults.object(forKey: "USERID"),"category_id":"1","subcategory_Id":"1","lat":gLatitude,"long":gLongitude]
//            
//            appConfig.fetchDataFromServer(header: "get_deals", withParameter: dict as NSDictionary, inVC: self) { (responce, staus) in
//                load.hide(delegate: self)
//                let status =  responce.value(forKey: "status")
//                if (status! as AnyObject).doubleValue == 1.0 {
//                    self.arrDealsList.removeAllObjects()
//                    var DealsArr = NSMutableArray()
//                    let DealsArrNew = responce["Deals"]! as! NSArray
//                    DealsArr = NSMutableArray(array: DealsArrNew)
//                    for dict in DealsArr {
//                        let model = ModelDeals()
//                        model.DealIDs   = (dict as AnyObject).value(forKey: "deal_Id") as? NSString
//                        model.DealName  = (dict as AnyObject).value(forKey: "deal_Name") as? NSString
//                        model.DealDesc  = (dict as AnyObject).value(forKey: "deal_desc") as? NSString
//                        model.Distance  = (dict as AnyObject).value(forKey: "distance") as? NSString
//                        model.ISFav     = (dict as AnyObject).value(forKey: "is_fav") as? Bool
//                        self.arrDealsList.add(model)
//                    }
//                    
//                    let theInfo: [AnyHashable: Any] = ["myArray" : self.arrDealsList]
//                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DEALS_AVAILABLE"), object: self, userInfo: theInfo)
//
//                }
//            }
//        } else {
//            load.hide(delegate: self)
//            appConfig.presentAlertWithTitle(title: kAPPName, message: "No internet connection.", vc: self)
//        }
//    }
    
    func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
    
    func didTapGoToLeft() {
        let currentIndex = pageMenu!.currentPageIndex
        
        if currentIndex > 0 {
            pageMenu!.moveToPage(currentIndex - 1)
        }
    }
    
    func didTapGoToRight() {
        let currentIndex = pageMenu!.currentPageIndex
        
        if currentIndex < pageMenu!.controllerArray.count {
            pageMenu!.moveToPage(currentIndex + 1)
        }
    }
    
    // MARK: - Container View Controller
    override var shouldAutomaticallyForwardAppearanceMethods : Bool {
        return true
    }
    
    override func shouldAutomaticallyForwardRotationMethods() -> Bool {
        return true
    }
    
    @IBAction func btnSideMenuPressed(_ sender: Any) {
        self.menuContainerViewController.toggleLeftSideMenuCompletion {
        }
    }
    
    @IBAction func btnChangeLocPressed(_ sender: Any) {
        
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        self.present(autocompleteController, animated: true, completion: nil)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
