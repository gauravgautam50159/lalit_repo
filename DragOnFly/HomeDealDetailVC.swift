//
//  HomeDealDetailVC.swift
//  DragonFly
//
//  Created by Suthar on 10/01/17.
//  Copyright © 2017 Octal. All rights reserved.
//

import UIKit
import Alamofire

class HomeDealDetailVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var viewScrollDownHeader: UIView!
    @IBOutlet weak var btnHeartFavHeader: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var scrollTableHeader: UIScrollView!
    @IBOutlet weak var viewTableHeader: UIView!
    @IBOutlet weak var tblDealDetail: UITableView!
    @IBOutlet weak var imgLogoHeader: UIImageView!
    @IBOutlet weak var lblDistanceHeader: UILabel!
    @IBOutlet weak var lblHeaderDealName: UILabel!
    @IBOutlet weak var btnRedeemFooter: UIButton!
    
    var StrDealIds = NSString()
    //var arrDealsList = NSMutableArray()
    var modelDeatil = ModelDealDetail()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
 
        self.automaticallyAdjustsScrollViewInsets = false
        self.tblDealDetail.separatorColor = UIColor.clear
        
       // arrDealsList = NSMutableArray()
        modelDeatil = ModelDealDetail()
        
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to: #selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        }
        
        btnRedeemFooter.isEnabled = false
        btnRedeemFooter.isUserInteractionEnabled = false
        btnRedeemFooter.setTitleColor(UIColor.lightGray, for: UIControlState.normal)

        fatchDealDetailsFromServer()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.menuContainerViewController.panMode = MFSideMenuPanModeNone
        self.navigationController?.isNavigationBarHidden = false
        UINavigationBar.appearance().backgroundColor = UIColor.black.withAlphaComponent(0.5)
        UINavigationBar.appearance().tintColor = UIColor.black.withAlphaComponent(0.5)
        self.navigationController?.navigationBar.barTintColor = UIColor.black.withAlphaComponent(0.5)
        self.navigationController?.navigationBar.backgroundColor = UIColor.black.withAlphaComponent(0.5)
    }
    
    func fatchDealDetailsFromServer () {

//        StrDealIds , "user_id": defaults.object(forKey: "USERID")
        if (Alamofire.NetworkReachabilityManager(host: "www.google.com")?.isReachable)! {
            load.show(views: self.view)
            let dict =  ["user_id": defaults.object(forKey: "USERID"),"deal_id":StrDealIds,"lat":gLatitude,"long":gLongitude]
            print(dict.values)
            appConfig.fetchDataFromServer(header: "get_deal_details", withParameter: dict as NSDictionary, inVC: self) { (responce, staus) in
                load.hide(delegate: self)
                let status =  responce.value(forKey: "status")
                if (status! as AnyObject).doubleValue == 1.0 {
                    
                    let dict = responce["data"] as! NSDictionary
                    
                    self.modelDeatil.address =  dict.value(forKeyPath: "business.address") as! NSString?//dict.object(forKey: "address") as! NSString?
                    self.modelDeatil.amount  =  dict.object(forKey: "amount") as! NSString?
                    self.modelDeatil.businessName = dict.value(forKeyPath: "business.business_name") as! NSString?//dict.value("business").value(forKey: "business_name")
                    self.modelDeatil.businessLogo = dict.value(forKeyPath: "business.logo") as! NSString?
                    self.modelDeatil.businessIds = dict.object(forKey: "business_id") as! NSString?
                    self.modelDeatil.CategoryIds = dict.object(forKey: "category") as! NSString?
                    self.modelDeatil.DealName = dict.object(forKey: "deal_name") as! NSString?
                    self.modelDeatil.DealDesc = dict.object(forKey: "description") as! NSString?
                    self.modelDeatil.dealIds = dict.object(forKey: "id") as! NSString?
                    self.modelDeatil.distances = NSString(format: "%@", dict.object(forKey: "distance") as! CVarArg) as NSString?//dict.object(forKey: "distance") as! NSString?
                    self.modelDeatil.ArrImages = NSMutableArray()
                    
                    let DealsImages = dict["images"]! as! NSArray
                    self.modelDeatil.ArrImages = NSMutableArray(array: DealsImages)
                    self.modelDeatil.isFavorite = dict.object(forKey: "is_fav") as? Bool
                    self.modelDeatil.PhoneNumber = dict.value(forKeyPath: "business.phone_number") as! NSString?
                    self.modelDeatil.loyaltyPoints = dict.object(forKey: "loyalty_points") as! NSString?
                    self.modelDeatil.ArrRecentDeals = NSMutableArray()
                    let DealsArr = dict["recent_deals"]! as! NSArray
                    
                    for dictrecent in DealsArr {
                        let model = ModelDeals()
                        model.DealIDs   = (dictrecent as! NSDictionary).value(forKey: "id") as? NSString
                        model.DealDesc  = (dictrecent as! NSDictionary).value(forKey: "description") as? NSString
                        model.DealName  = (dictrecent as! NSDictionary).value(forKey: "deal_name") as? NSString
                        model.DealImg   = (dictrecent as! NSDictionary).value(forKey: "logo") as? NSString
                        self.modelDeatil.ArrRecentDeals?.add(model)
                    }
                    self.modelDeatil.SubCategoryIds = dict.object(forKey: "sub_category") as? NSString
                    print(self.modelDeatil)
                    self.makeTableHeaderViewData ()
                }
                self.tblDealDetail.reloadData()
            }
        } else {
            load.hide(delegate: self)
            appConfig.presentAlertWithTitle(title: kAPPName, message: "No internet connection.", vc: self)
        }
    }
    
    
    func makeTableHeaderViewData () {
        self.tblDealDetail.setContentOffset(CGPoint.zero, animated: true)
        var width : CGFloat = 0.0
        for StrImg in modelDeatil.ArrImages! {
            print(StrImg)
            let imageV : UIImageView = UIImageView(frame: CGRect(origin: CGPoint(x: width,y :0), size: CGSize(width: SCREEN_WIDTH, height: viewTableHeader.frame.size.height-viewScrollDownHeader.frame.size.height)))
            imageV.setImageWithUrl(NSURL(string: StrImg as! String) as! URL, placeHolderImage: UIImage(named: "inner_slider"))
            imageV.backgroundColor = UIColor.red
            scrollTableHeader.addSubview(imageV)
            width = width+SCREEN_WIDTH
        }
//        defaults.object(forKey: "LOYALTYPOINTS")
        
        lblHeaderDealName.text = modelDeatil.businessName as String?
        lblDistanceHeader.text = NSString(format: "%0.2f Miles",(modelDeatil.distances?.floatValue)!) as String
        
        let loyaltyTotal = defaults.object(forKey: "LOYALTYPOINTS") as! NSString
        if (modelDeatil.loyaltyPoints?.floatValue)! > loyaltyTotal.floatValue {
         btnRedeemFooter.isEnabled = false
         btnRedeemFooter.isUserInteractionEnabled = false
         btnRedeemFooter.setTitleColor(UIColor.lightGray, for: UIControlState.normal)
        } else {
         btnRedeemFooter.isEnabled = true
         btnRedeemFooter.isUserInteractionEnabled = true
         btnRedeemFooter.setTitleColor(UIColor(red: 0.0/255.0, green: 136.0/255.0, blue: 206.0/255.0, alpha: 1.0), for: UIControlState.normal)
        }
        
        if modelDeatil.isFavorite! {
            btnHeartFavHeader.isSelected = true
        }else{
            btnHeartFavHeader.isSelected = false
        }
        
        scrollTableHeader.contentSize = CGSize(width: width, height: viewTableHeader.frame.size.height-viewScrollDownHeader.frame.size.height)
        pageControl.numberOfPages = (modelDeatil.ArrImages?.count)!
        tblDealDetail.reloadData()
    }
    
    @IBAction func btnFavUnFavPressed(_ sender: Any) {
        NSLog("pressed!")
        if (Alamofire.NetworkReachabilityManager(host: "www.google.com")?.isReachable)! {
            load.show(views: self.view)
            let dict : Dictionary = ["user_id": defaults.object(forKey: "USERID"),"deal_id":modelDeatil.dealIds!,"is_fav":modelDeatil.isFavorite!]
            print(dict.values as Any)
            appConfig.fetchDataFromServer(header: "fav_deal", withParameter: dict as NSDictionary, inVC: self) { (responce, staus) in
                load.hide(delegate: self)
                let status =  responce.value(forKey: "status")
                if (status! as AnyObject).doubleValue == 1.0 {
                    self.modelDeatil.isFavorite = responce.value(forKey: "is_fav")! as? Bool
                    if self.modelDeatil.isFavorite! {
                        self.btnHeartFavHeader.isSelected = true
                    }else{
                        self.btnHeartFavHeader.isSelected = false
                    }
                }
            }
        } else {
            load.hide(delegate: self)
            appConfig.presentAlertWithTitle(title: kAPPName, message: "No internet connection.", vc: self)
        }
    }
    
    @IBAction func pageControlTapped(_ sender: Any) {
        let indexOfPage : CGFloat = CGFloat(pageControl.currentPage) //CGFloat(scrollTableHeader.contentOffset.x / scrollTableHeader.frame.size.width)
        switch pageControl.currentPage {
        case 0:
            scrollTableHeader.contentOffset.x = 0
        default:
            scrollTableHeader.contentOffset.x = indexOfPage*SCREEN_WIDTH
        }
    }
    
    // MARK: ScrollView Methods
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let indexOfPage = CGFloat(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(indexOfPage)
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
  
    @IBAction func shareBtnPressed(_ sender: Any) {
        
    }
   
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to: #selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = nil
        }
        UINavigationBar.appearance().backgroundColor = nil
        UINavigationBar.appearance().tintColor = nil
        self.navigationController?.navigationBar.barTintColor = nil
        self.navigationController?.navigationBar.backgroundColor = nil
    }
    
    // MARK: - Table view data source & delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row > 0 && indexPath.row < 4{
            return 38.0
        }
        else if indexPath.row == 4 {
            return  142.0
        }
        else if indexPath.row == 5 {
            return  86.0
        }
        else{
          return 48
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if modelDeatil.ArrRecentDeals != nil {
            if (modelDeatil.ArrRecentDeals?.count)! > 0 {
                return 6
            }else {
                return 5
            }
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "AddressTableCell", for: indexPath)
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        }
        else if indexPath.row > 0 && indexPath.row < 3 {
            let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "PhoneTableCell", for: indexPath)
            //54 55
            
            let lblTitle = cell.viewWithTag(54) as! UILabel
            let lblDesc = cell.viewWithTag(55) as! UILabel
            
            if indexPath.row == 1 {
                lblTitle.text = "Phone"
                lblDesc.text = modelDeatil.PhoneNumber as String?
            }else if indexPath.row == 2 {
                lblTitle.text = "Hours of operations"
                lblDesc.text = ""
            }
            
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        }
        else if indexPath.row == 3{
            let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "MoreTableCell", for: indexPath)
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        }
        if modelDeatil.ArrRecentDeals != nil {
            if (modelDeatil.ArrRecentDeals?.count)! > 0 {
                if indexPath.row == 4 {
                    let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "CollectionTableCell", for: indexPath)
                    let collectionView : UICollectionView =  cell.viewWithTag(78) as! UICollectionView
                    collectionView.register(UINib(nibName: "DealCollectionCell", bundle: nil), forCellWithReuseIdentifier: "DealCollectionCell")
                    collectionView.reloadData()
                    
                    cell.selectionStyle = UITableViewCellSelectionStyle.none
                    return cell
                    
                }else{
                    let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "LoyaltyCell", for: indexPath)
                    //
                    let lblPoints = cell.viewWithTag(81) as! UILabel
                    lblPoints.text = NSString(format: "%@ Points Earned",modelDeatil.loyaltyPoints!) as String
                    
                    cell.selectionStyle = UITableViewCellSelectionStyle.none
                    return cell
                }
            }
            else{
                let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "LoyaltyCell", for: indexPath)
                //
                let lblPoints = cell.viewWithTag(81) as! UILabel
                lblPoints.text = NSString(format: "%@ Points Earned",modelDeatil.loyaltyPoints!) as String
                
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
            }
        }
        else{
            let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "LoyaltyCell", for: indexPath)
            //
            let lblPoints = cell.viewWithTag(81) as! UILabel
            lblPoints.text = NSString(format: "%@ Points Earned",modelDeatil.loyaltyPoints!) as String
            
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        }
    }
    
    @IBAction func btnFooterReddemPressed(_ sender: Any) {
        
//        /redeem_loyalty_points 
//        modelDeatil.dealIds
        
        if (Alamofire.NetworkReachabilityManager(host: "www.google.com")?.isReachable)! {
                load.show(views: self.view)
                let dict : Dictionary = ["id": modelDeatil.dealIds]
                print(dict.values)
                appConfig.fetchDataFromServer(header: "redeem_loyalty_points", withParameter: dict as NSDictionary, inVC: self) { (responce, staus) in
                    load.hide(delegate: self)
                    print(responce)
                    let status =  responce.value(forKey: "status")
                    if (status! as AnyObject).doubleValue == 1.0 {
                        appConfig.presentAlertWithTitle(title: kAPPName, message: responce.object(forKey: "message") as! String, vc: self)
                    }
                }
        } else {
            load.hide(delegate: self)
            appConfig.presentAlertWithTitle(title: kAPPName, message: "No internet connection.", vc: self)
        }
        
    }
    
    // MARK: - Collection view data source & delegate Methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if modelDeatil.ArrRecentDeals != nil {
            return (modelDeatil.ArrRecentDeals?.count)!
        }else{
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: SCREEN_WIDTH, height: 110)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let modelDeal = modelDeatil.ArrRecentDeals?[indexPath.row] as! ModelDeals
        let cell : DealCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DealCollectionCell", for: indexPath) as! DealCollectionCell
        
//        btnGetDeal
        cell.lblDealDescription.text = modelDeal.DealDesc as String?
        cell.imgDealCollection.setImageWithUrl(NSURL(string: modelDeal.DealImg as! String) as! URL, placeHolderImage: UIImage(named: "inner_slider"))
        
        cell.btnGetDeal.tag = indexPath.row
        cell.btnGetDeal.addTarget(self, action: #selector(BtnGetDealPressed(button:)), for: .touchUpInside)
        
        return cell
    }
    
    func BtnGetDealPressed(button: UIButton) {
        let modelDeal = modelDeatil.ArrRecentDeals?[button.tag] as! ModelDeals
        StrDealIds  = modelDeal.DealIDs!
        modelDeatil = ModelDealDetail()
        fatchDealDetailsFromServer()
    }
   
//    {}

   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt insetForSectionAtion: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
